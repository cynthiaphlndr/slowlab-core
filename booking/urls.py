from django.urls import include, path
from .views import bookingForm, index, get_booking, get_lokasi, get_jenis, admin

urlpatterns = [
    path('', index, name='index'),
    path('booking-form/', bookingForm, name='booking form'),
    path('get-booking/', get_booking, name='get booking'),
    path('get-lokasi/', get_lokasi, name='get lokasi'),
    path('get-jenis/', get_jenis, name='get jenis'),
    path('admin/', admin, name='admin'),
]
