from django.http import response
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, JsonResponse
from django.core import serializers
from django.contrib.auth.decorators import login_required
from .models import Booking, JenisTes
from Lokasi.models import Lokasi
from .forms import BookingForm

# Create your views here.


@login_required(login_url='/auth/login')
def index(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = BookingForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            return HttpResponseRedirect('/booking')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = BookingForm()

    return render(request, 'index.html', {'form': form})


@ login_required(login_url='/admin')
def admin(request):
    return render(request, 'admin.html')


def get_booking(request):
    data = serializers.serialize('json', Booking.objects.all())
    return HttpResponse(data, content_type="application/json")


def get_lokasi(request):
    data = serializers.serialize('json', Lokasi.objects.all())
    return HttpResponse(data, content_type="application/json")


def get_jenis(request):
    data = serializers.serialize('json', JenisTes.objects.all())
    return HttpResponse(data, content_type="application/json")


def bookingForm(request):
    if request.method == 'POST':
        form = BookingForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/booking-form')
    else:
        form = BookingForm()

    return render(request, 'form.html', {'form': form})
