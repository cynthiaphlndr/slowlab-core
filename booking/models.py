from django.db import models
from django.db.models.deletion import CASCADE
from django.utils import timezone

# Create your models here.


class JenisTes(models.Model):
    jenis_id = models.CharField(max_length=30, unique=True)
    nama = models.CharField(max_length=30)

    def __str__(self):
        return self.nama


class Booking(models.Model):
    email = models.CharField(max_length=30)
    nama_lengkap = models.CharField(max_length=30)
    nomor_wa = models.CharField(max_length=30)
    lokasi = models.CharField(max_length=30)
    tanggal = models.DateField(default=timezone.now)
    jenis = models.CharField(max_length=30)

    def __str__(self):
        return self.tanggal + " " + self.email
