from django.http import response, JsonResponse
from django.shortcuts import render
from datetime import datetime, date
from django.http.response import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
import cek_hasil
from cek_hasil.forms import TestForm
from .models import Result, Test
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

def index(request):
    form = TestForm()

    # if (request.method == "POST" and form.is_valid):
    #     if 'submit_button' in request.POST:
    #         hasil = Result.objects.filter(cek_no_test=request.POST['no_test'])
    #         response = {'hasil':hasil}
    #         return render(request, "cek_hasil.html", response)

    if (request.method == "GET" and form.is_valid):       
        if 'reset_button' in request.GET:
            hasil = Result.objects.all()
            response = {'hasil':hasil}
            form.save()
            return render(request, "cek_hasil.html", response)

    hasil = Result.objects.all()
    response = {'hasil':hasil}
    return render(request, "cek_hasil.html", response)

    # context['form']= form
    # return render(request, "cek_hasil.html", context)

@csrf_exempt
def allData(request):
    data = serializers.serialize('json', Result.objects.all())
    return JsonResponse(data, content_type="application/json", safe=False)