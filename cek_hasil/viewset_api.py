from rest_framework.serializers import Serializer
from cek_hasil.models import Result
from cek_hasil.serializers import ResultSerializer
from rest_framework import viewsets

class ResultViewset(viewsets.ModelViewSet):
    queryset = Result.objects.all()
    serializer_class = ResultSerializer