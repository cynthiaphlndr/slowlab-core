from django.db.models import fields
from cek_hasil.models import Result
from rest_framework import serializers

class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = Result
        fields = ['cek_no_test', 'status', 'nama', 'no_ktp', 'tanggal_tes']