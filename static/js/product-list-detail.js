console.log('hello world')

const url = window.location.href
const searchForm = document.getElementById('search-form')
const searchInput = document.getElementById('search-input')
const resultsBox = document.getElementById('results-box')

const csrf = document.getElementsByName('csrfmiddlewaretoken')[0].value

var formatter = new Intl.NumberFormat('id-ID', {
    style: 'currency',
    currency: 'IDR',
  
    // These options are needed to round to whole numbers if that's what you want.
    // minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
  });

const sendSearchData = (produk) => {
    $.ajax({
        type: 'POST',
        url: 'search/',
        data: {
            'csrfmiddlewaretoken': csrf,
            'produk': produk,
        },
        success: (res)=> {
            console.log(res.data)
            const data = res.data
            if(Array.isArray(data)) {
                resultsBox.innerHTML = ""
                data.forEach(produk=> {
                    resultsBox.innerHTML += `
                        <a href="../${produk.pk}" class="item" style="width: 100%>
                            <div class=" mt-2 mb-2">
                                <div>
                                    <h5 style="width: 100%;">${produk.name}</h5>
                                    <p class="text-muted">${formatter.format(produk.price)}</p>
                                </div>
                            </d>
                        </a>
                    `  
                })
            } else {
                if (searchInput.value.length > 0) {
                    resultsBox.innerHTML = `<b> ${data} </b>`
                } else {
                    resultsBox.classList.add('not-visible')
                }
            }
        },
        error: (err)=> {
            console.log(err)
        }
    })
}


searchInput.addEventListener('keyup', e=> {
    console.log(e.target.value)

    if (resultsBox.classList.contains('not-visible')) {
        resultsBox.classList.remove('not-visible')
    }

    sendSearchData(e.target.value)
})