from .models import Lokasi
from django import forms
import datetime as dt

class FormLokasi(forms.ModelForm):
    class Meta:
        model = Lokasi
        fields = "__all__"
