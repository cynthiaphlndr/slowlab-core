from django.shortcuts import render, get_object_or_404
from .models import DataCovid
import requests
from django.http.response import HttpResponse
from django.core import serializers 
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.
def index(request):
    DataCovid.objects.all().delete()

    all_data = {}
    url = 'https://data.covid19.go.id/public/api/prov.json'
    response = requests.get(url)
    data = response.json()
    prov = data['list_data']

    for i in prov:
        data_prov = DataCovid(
            name = i['key'],
            total_kasus = i['jumlah_kasus'],
            meninggal = i['jumlah_meninggal'],
            sembuh = i['jumlah_sembuh'],
            kasus_aktif = i['jumlah_dirawat'],
            last_date = data['last_date'],
            penambahan_tk = i['penambahan']['positif'],
            penambahan_m = i['penambahan']['meninggal'],
            penambahan_s = i['penambahan']['sembuh'],
            penambahan_ka = int(i['penambahan']['positif']) - int(i['penambahan']['meninggal']) - int(i['penambahan']['sembuh']),
        )
        data_prov.save()

    all_data = DataCovid.objects.all().order_by('-id')

    return render(request, 'home_index.html', {'all_data':all_data})

def getData(request):
    data = serializers.serialize('json', DataCovid.objects.all())
    return HttpResponse(data, content_type="application/json")

# def getDataProv(request, name):
#     data_prov = serializers.serialize('json',DataCovid.objects.get(name=name))
#     return HttpResponse(data_prov, content_type="application/json")

# def product_detail(request, pk):
#     data = get_object_or_404(DataCovid, pk=pk)
#     return render(request, 'detail_produk.html', {'data': data})