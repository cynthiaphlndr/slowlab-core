from django.urls import path
from user_auth.views import login_page, flutter_login, get_province, get_uname_email, flutter_profile_edit, flutter_profile, flutter_register, logout_user, register_page, user_profile_page, profile_settings, uname_verif, email_verif

urlpatterns = [
    path('login', login_page, name='login'),
    path('logout', logout_user, name='logout_user'),
    path('register', register_page, name='register'),
    path('profile', user_profile_page, name='user_profile'),
    path('profile/settings', profile_settings, name='profile_settings'),

    path('ajax/email_verif', email_verif, name='email_verif'),
    path('ajax/uname_verif', uname_verif, name='uname_verif'),

    path('flutter/login', flutter_login, name='flutter_login'),
    path('flutter/register', flutter_register, name='flutter_register'),

    path('flutter/profile', flutter_profile, name='flutter_profile'),
    path('flutter/profile/edit', flutter_profile_edit, name='flutter_profile_edit'),

    path('flutter/get_province', get_province, name='get_province'),
    path('flutter/get_uname_email', get_uname_email, name='get_uname_email'),
]