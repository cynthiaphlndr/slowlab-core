from typing import ContextManager
from django.contrib.auth.models import Group
from django.core.files.base import ContentFile
from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.http import JsonResponse
from django.core import serializers

import json
import re

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout

from .forms import profile_form, user_regist_form
from django.contrib import messages

# User Role Based Permissions & Authentication
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from user_auth.decorators import unauthenticated_user, allowed_user

from .models import *

from django.shortcuts import render
from django.contrib.auth import authenticate, login as auth_login
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@unauthenticated_user
def login_page(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('user_profile')
        else:
            messages.info(request, 'username or password is incorrect')

    context = {}
    return render(request, "login.html", context)

def logout_user(request):
    logout(request)
    return redirect('login')

@unauthenticated_user
def register_page(request):
    form = user_regist_form()

    if request.method == 'POST':
        form = user_regist_form(request.POST)
        if form.is_valid():
            user = form.save()
            uname = form.cleaned_data.get('username')

            user_profile.objects.create(
				user=user,
				)

            messages.success(request, uname + " has been Created, Please Login")

            return redirect('login')
        else:
            messages.info(request, 'Registrasi Gagal, Silahkan periksa kembali Username, Email dan Password anda')

    context = {'form' : form}
    return render(request, "register.html", context)


@login_required(login_url='login')
def user_profile_page(request):
    username = request.user.username
    email = request.user.email
    full_name = request.user.user_profile.full_name
    bod =  request.user.user_profile.BOD
    phone = request.user.user_profile.phone
    province = request.user.user_profile.province
    address = request.user.user_profile.address

    context = {
        'username': username,
        'email': email,
        'full_name': full_name,
        'bod': bod,
        'phone':phone,
        'province': province,
        'address': address,
        }
    
    return render(request, "user_profile.html", context)

@login_required(login_url='login')
def profile_settings(request):
    user_profile = request.user.user_profile
    form = profile_form(instance=user_profile)

    if request.method == "POST":
        form = profile_form(request.POST, instance=user_profile)
        if form.is_valid():
            form.save()
            return redirect('user_profile')

    context = {'form': form}
    return render(request, 'user_profile_edit.html', context)

def uname_verif(request):
    if request.method == 'GET':
        uname = request.GET.get('username')
        user = User.objects.filter(username=uname)
        if user:
            return JsonResponse({'user': False})
        return JsonResponse({'user': True})

def email_verif(request):
    if request.method == 'GET':
        user_email = request.GET.get('email')
        mail = User.objects.filter(email=user_email)
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        if(re.fullmatch(regex, user_email)):
            if not mail:
                return JsonResponse({'email': True})
        return JsonResponse({'email': False})

@csrf_exempt
def flutter_login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            auth_login(request, user)
            # Redirect to a success page.
            return JsonResponse({
              "status": True,
              "message": "Successfully Logged In!"
            }, status=200)
        else:
            return JsonResponse({
              "status": False,
              "message": "Failed to Login, Account Disabled."
            }, status=401)

    else:
        return JsonResponse({
          "status": False,
          "message": "Failed to Login, check your email/password."
        }, status=401)

@csrf_exempt
def flutter_register(request):

    if (request.method == 'POST'):

        data = json.loads(request.body)

        username = data["username"]
        email = data["email"]
        password1 = data["password1"]

        user = User.objects.create_user(
        username = username, 
        email = email,
        password = password1
        )

        user_profile.objects.create(
			user=user,
		)

        return JsonResponse({'status': 200})

@csrf_exempt
def flutter_profile_edit(request):

    if (request.method == 'POST'):

        data = json.loads(request.body)

        request.user.user_profile.full_name = data["fullname"]
        request.user.user_profile.BOD = data["bod"]
        request.user.user_profile.phone = data["phone"]
        request.user.user_profile.address = data["address"]

        for p in Province.objects.all():
            if p.name == data["province"]:
               request.user.user_profile.province = p 

        request.user.user_profile.save()

        return JsonResponse({'status': 200})

@csrf_exempt
def flutter_profile(request):
 
    if (request.user.user_profile.province != None):
        province = request.user.user_profile.province.name
    else:
        province = "none"
    

    userdata={
        "username" : request.user.username,
        "email" : request.user.email,
        "fullname" : request.user.user_profile.full_name,
        "bod" : request.user.user_profile.BOD,
        "phone" : request.user.user_profile.phone,
        "province" : province,
        "address" : request.user.user_profile.address
    }

    return JsonResponse(userdata)

@csrf_exempt
def get_province(request):

    provinceLst = []
    
    for p in Province.objects.all():
        provinceLst.append(p.name)
    # data = serializers.serialize('json', Province.objects.all())
    
    province_data={
        "allprovince" : provinceLst,
    }

    return JsonResponse(province_data)

@csrf_exempt
def get_uname_email(request):

    userdata={
        "username" : request.user.username,
        "email" : request.user.email,
    }
    
    return JsonResponse(userdata)