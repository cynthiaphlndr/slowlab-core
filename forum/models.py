from django.db import models

# Create your models here.
#parent model
class Pertanyaan(models.Model):
    nama = models.CharField(max_length=200, default="Anonymous", blank=True)
    pertanyaan = models.CharField(max_length=1000, blank=True)
    nama_admin = models.CharField(max_length=200, default="Admin")
    jawaban = models.CharField(max_length=1000, default="Pertanyaan ini belum memiliki jawaban.")