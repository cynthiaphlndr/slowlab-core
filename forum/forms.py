from django import forms
from django.forms import ModelForm
from .models import *
 
class InputBertanya(ModelForm):
    class Meta:
        model= Pertanyaan
        fields = "__all__"
        exclude = ('nama_admin', 'jawaban')