# from django.shortcuts import render

# # Create your views here.
# def index(request):
#     return render(request, 'forum.html')


from django.http.response import JsonResponse
from django.shortcuts import render,redirect
from .models import * 
from .forms import * 
 
def index(request):

    isi_forum = Pertanyaan.objects.all()
    form = InputBertanya(request.POST or None)

    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/forum')

    context={'isi_forum':isi_forum,
             'form':form}

    return render(request,'forum.html',context)
    
def getForum(request):
        forum = Pertanyaan.objects.all()
        return JsonResponse({"forum" : list(forum.values())})