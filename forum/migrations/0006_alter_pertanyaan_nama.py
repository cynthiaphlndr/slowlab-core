from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0005_auto_20211026_0307'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pertanyaan',
            name='nama',
            field=models.CharField(blank=True, default='Anonymous', max_length=200),
        ),
    ]
