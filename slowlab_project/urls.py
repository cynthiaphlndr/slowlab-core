"""slowlab_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from cek_hasil.views import index as index_cek_hasil
import booking.urls as booking
import cek_hasil.urls as cek_hasil
import home.urls as home
import user_auth.urls as user_auth
import Lokasi.urls as lokasi
import forum.urls as forum
import product_list.urls as product_list

urlpatterns = [
    path('admin/', admin.site.urls),
    path('booking/', include(booking)),  # route to booking page
    path('cek-hasil/', include(cek_hasil)),  # route to cek-hasil page
    path('', include(('home.urls', 'home'), namespace='home')),
    path('auth/', include(user_auth)),
    path('lokasi/', include(lokasi)),
    re_path(r'^$', index_cek_hasil, name='index'),
    path('forum/', include(forum)),

    path('product-list/', include(product_list))
]
